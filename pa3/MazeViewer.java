//Name:Yi-An Chen
//USC loginid:4558-3114-48
//CS 455 PA3
//Fall 2015

import java.io.FileNotFoundException;
import java.io.IOException;
import javax.swing.JFrame;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Scanner;
/**
 * MazeViewer class
 * 
 * Program to read in and display a maze and a path through the maze. At user
 * command displays a path through the maze if there is one.
 * 
 * How to call it from the command line:
 * 
 *      java MazeViewer mazeFile
 * 
 * where mazeFile is a text file of the maze. The format is the number of rows
 * and number of columns, followed by one line per row. Each maze location is
 * either a wall (1) or free (0). Here is an example of contents of a file for
 * a 3x4 maze:
 * 
 * 3 4 
 * 0111
 * 0000
 * 1110
 * 0010
 * 
 * The top left is the maze entrance, and the bottom right is the maze exit.
 * 
 */

public class MazeViewer {
   
   public static void main(String[] args)  {

      String fileName = "";

      try {

         if (args.length < 1) {
            System.out.println("ERROR: missing file name command line argument");
         }
         else {
            fileName = args[0];

            boolean[][] mazeData = readMazeFile(fileName);

            JFrame frame = new MazeFrame(mazeData);

            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

            frame.setVisible(true);
         }

      }
      catch (FileNotFoundException exc) {
         System.out.println("File not found: " + fileName);
      }
      catch (IOException exc) {
         exc.printStackTrace();
      }
   }

   /**
   readMazeFile reads in and returns a maze from the file whose name is
   String given. The file format is shown in the MazeViewer class comments.
   
   @param fileName
             the name of a file to read from
   @returns 
            the array with maze contents. false at a location means there is no wall
            (0 in the file) and true means there is a wall (1 in the file).
            The first dimension is which row, and the second is which column. E.g. if the file
            started with 3 10, it would mean the array returned would have
            3 rows, and 10 columns.
   @throws FileNotFoundException
              if there's no such file (subclass of IOException)
   @throws IOException
              (hook given in case you want to do more error-checking.
               that would also involve changing main to catch other exceptions)
   */
    private static boolean[][] readMazeFile(String fileName) throws IOException {
	int numRow = 0;                       
	int numCol = 0;                      
	int currRow = 0;                     // After reading first line, this would be used to calculate the current row of 2D Array
	FileReader file = new FileReader(fileName);
	BufferedReader br = new BufferedReader(file);
	String line = null;
	
	// read first line 
	if ((line = br.readLine()) !=null){
	    String [] number = line.split(" ");
	    numRow = Integer.valueOf(number[0]);      //by reading first line, give the maze the row
	    numCol = Integer.valueOf(number[1]);      //by reading first line, give the maze the col
	}
	boolean [][] maze = new boolean[numRow][numCol];
	
	//read the line after first line, make the integer array convert into boolean array
	while ((line = br.readLine()) !=null){
	    for (int i =0;i<numCol;i++){
		int ArrVal = line.charAt(i)-'0';
     		if (ArrVal!=0){
		    maze[currRow][i]=true;
		}
    		else{
	    	    maze[currRow][i]=false;
	   	}
	    }
	currRow++;
     	}
		br.close();		
		return maze;   // DUMMY CODE TO GET IT TO COMPILE
    }
}
