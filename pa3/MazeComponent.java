// Name:Yi-An Chen
// USC loginid:4558-3114-48
// CS 455 PA3
// Fall 2015

import java.awt.Color;
import java.awt.Graphics;
import java.util.ListIterator;
import java.awt.geom.*;
import java.awt.Graphics2D;

import javax.swing.JComponent;

/**
   MazeComponent class
   
   A component that displays the maze and a path through it if one has been found.
*/
public class MazeComponent extends JComponent
{
   private Maze maze;
   //private LinkedList<MazeCoord> path;
   private static final int START_X = 10; // where to start drawing maze in frame
   private static final int START_Y = 10;
   
   private static final int BOX_WIDTH = 20;  // width and height of one maze unit
   private static final int BOX_HEIGHT = 20;
   

   /**
      Constructs the component.
      @param maze   the maze to display
   */
   public MazeComponent(Maze maze) 
   {      
       this.maze = maze;
	   
	   
   }

   
   /**
     Draws the current state of maze including a path through it if one has
     been found.
     @param g the graphics context
   */
   public void paintComponent(Graphics g)
   {
       // draw maze structure with black wall and white path.
       g.drawRect(START_X, START_Y, maze.numCols()*BOX_HEIGHT, maze.numRows()*BOX_HEIGHT);
       g.setColor(Color.black);
       for (int col=0;col<maze.numCols();col++){
	   for (int row=0;row<maze.numRows();row++){
	       if (maze.hasWallAt(new MazeCoord (row,col))==true){
      		   g.fillRect(START_X + (col)*BOX_WIDTH, START_Y + (row)*BOX_HEIGHT, BOX_WIDTH, BOX_HEIGHT);
	       }
	   }		   
       }
       //If there is exit in the maze, draw a green box at exit.
       if ( !maze.hasWallAt(new MazeCoord (maze.numRows()-1,maze.numCols()-1))){
	   g.setColor(Color.green);
	   g.fillRect(START_X+(maze.numCols()-1)*BOX_WIDTH, START_Y+(maze.numRows()-1)*BOX_HEIGHT, BOX_WIDTH, BOX_HEIGHT);
       }
       //Draw a line if there can find a way to exit.
       Graphics2D g2=(Graphics2D) g;
       g2.setColor(Color.blue);
       ListIterator<MazeCoord> iter=maze.getPath().listIterator();
       
       Point2D from=new Point2D.Double(START_X+BOX_WIDTH/2,START_Y+BOX_HEIGHT/2);
       
       while(iter.hasNext()){
	   MazeCoord curr = iter.next();
	   Point2D to = new Point2D.Double((curr.getCol()+1)*BOX_WIDTH, +(curr.getRow()+1)*BOX_HEIGHT);
	   Line2D.Double segment = new Line2D.Double(from, to);
	   g2.draw(segment);
	   from = to;
       }
   }
}




