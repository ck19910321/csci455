// Name:Yi-An Chen
// USC loginid:4558-3114-48
// CS 455 PA3
// Fall 2015


import java.util.LinkedList;


/**
   Maze class

   Stores information about a maze and can find a path through the maze
   (if there is one).

   Assumptions about structure of the mazeData (parameter to constructor), and the
   path:
     -- no outer walls given in mazeData -- search assumes there is a virtual 
        border around the maze (i.e., the maze path can't go outside of the maze
        boundaries)
     -- start location for a path is maze coordinate (START_SEARCH_ROW,
        START_SEARCH_COL) (constants defined below)
     -- exit loc is maze coordinate (numRows()-1, numCols()-1) 
           (methods defined below)
     -- mazeData input 2D array of booleans, where true means there is a wall
        at that location, and false means there isn't (see public FREE / WALL 
        constants below) 
     -- in mazeData the first index indicates the row. e.g., mazeData[row][col]
     -- only travel in 4 compass directions (no diagonal paths)
     -- can't travel through walls
 */

public class Maze {
   private boolean[][] MazeD;
   private LinkedList<MazeCoord> path;
   private LinkedList<MazeCoord> PassedPath;
 
   public static final int START_SEARCH_COL = 0;
   public static final int START_SEARCH_ROW = 0;
   
   public static final boolean FREE = false;
   public static final boolean WALL = true;

   /**
      Constructs a maze.
      @param mazeData the maze to search.  See general Maze comments for what
      goes in this array.

    */
   public Maze(boolean[][] mazeData)
   {
      MazeD =  mazeData;
      path = new LinkedList<MazeCoord>();
      PassedPath = new LinkedList<MazeCoord>();
   }
   

   /**
   Returns the number of rows in the maze
   @return number of rows
   */
   public int numRows() {
      return MazeD.length;   // DUMMY CODE TO GET IT TO COMPILE
   }

   /**
   Returns the number of columns in the maze
   @return number of columns
   */   
   public int numCols() {
      return MazeD[0].length;   // DUMMY CODE TO GET IT TO COMPILE
   } 
 
   
   /**
      Returns true iff there is a wall at this location
      @param loc the location in maze coordinates
      @return whether there is a wall here
      PRE: 0 <= loc.getRow() < numRows() and 0 <= loc.getCol() < numCols()
   */
   public boolean hasWallAt(MazeCoord loc) {
      if (loc.getRow()<0||loc.getCol()<0 ||
	  loc.getRow()>=numRows()||loc.getCol()>= numCols()
	  ||(MazeD[loc.getRow()][loc.getCol()]==WALL))
      return true;   
	  else return false;
   }

   
   /**
      Returns path through the maze. First element is starting location, and
      last element is exit location.  If there was not a path, or if this is called
      before search, returns empty list.

      @return the maze path
    */
   public LinkedList<MazeCoord> getPath() {
       if (path.size()!=0){
	   LinkedList<MazeCoord> ReversePath = new LinkedList<MazeCoord>(); //According to my recursive method, 
	   for (int i = path.size()-1;i>0;i--){				    //the way to recorded the path is from end point to start point.
	       ReversePath.add(path.get(i));		 		    //So I use this for loop to reverse the sequence.
	   }
	   ReversePath.addFirst(new MazeCoord(START_SEARCH_ROW, START_SEARCH_COL));
	   return ReversePath;   
       }
       else return path;
       }
   /**
    * Return iff the current location has been visited by recording the way that has been gone through.
    * @param loc current location
    * @return Return iff the current location had been visited by recording the way that has been gone through.
    */
   public boolean hasPassedPath( MazeCoord loc){
	   for (int i = 0; i < PassedPath.size(); i++){
	       if (PassedPath.get(i).getRow()==loc.getRow() && PassedPath.get(i).getCol()==loc.getCol()){
		   return true;
	       }	  
	   }
	   return false;
   }


   /**
      Find a path through the maze if there is one.
      @return whether path was found.
    */
   public boolean search()  {      
       path.clear();    //reset the data if calling search again
       PassedPath.clear();
       return RecursiveSearch ( START_SEARCH_ROW, START_SEARCH_COL);  // DUMMY CODE TO GET IT TO COMPILE
   }
   /**
    * The recursive helper method would move the current location till it finds the end. 
    * If there is no path to exit or no exit in the maze, it will not add any path.
    * @param CurRow the row of current location
    * @param CurCol the column of current location
    * @return iff there is any way to the exit. 
    */
   private boolean RecursiveSearch(int CurRow, int CurCol){
       if (CurRow == numRows()-1 && CurCol ==numCols()-1 && !hasWallAt(new MazeCoord (CurRow,CurCol))){     //Base case if there is a way to the exit 
	   MazeCoord NewLoc = new MazeCoord(CurRow,CurCol);
	   path.add(NewLoc);
	   return true;
       }
       if (hasWallAt(new MazeCoord (CurRow,CurCol)) ||hasPassedPath(new MazeCoord(CurRow,CurCol))){         //Base case if there is a wall
	   return false;
       }
       MazeCoord PassedLoc = new MazeCoord(CurRow, CurCol);
       PassedPath.add(PassedLoc);
       if (RecursiveSearch( CurRow + 1, CurCol)){                                                           //Recursive case: move down
	   MazeCoord NewLoc = new MazeCoord(CurRow + 1,CurCol);
	   path.add(NewLoc);
	   return true;
       }
       else if (RecursiveSearch(CurRow - 1, CurCol)){                                                       //Recursive case: move up
	   MazeCoord NewLoc = new MazeCoord(CurRow - 1,CurCol);
	   path.add(NewLoc);
	   return true;
       }
       else if (RecursiveSearch( CurRow, CurCol + 1)){                                                      //Recursive case: move right 
	   MazeCoord NewLoc = new MazeCoord(CurRow,CurCol + 1);
	   path.add(NewLoc);
	   return true;
       }
       else if (RecursiveSearch(CurRow, CurCol - 1)){                                                       //Recursive case: move left
	   MazeCoord NewLoc = new MazeCoord(CurRow,CurCol - 1);
	   path.add(NewLoc);
	   return true;
       }
       else return false;
   }
}
