//Name:Yi-An Chen
//USC loginid:4558-3114-48
//CS 455 PA4
//Fall 2015
import java.util.ArrayList;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Scanner;

/**
 * include main to read file and then generate words writing  into file
 * there are two options to run this class
 * 1. write command-line argument as: -d prefixLength numWords sourceFile outputFile, which would run Debug Mode
 * 2. write command-line argument as: prefixLength numWords sourceFile outputFile, which would run without Debug Mode
 * @author Yi-An Chen
 *
 */
public class GenText {
	// constant default max chars per line at output file
	public static final int MAX_PER_LINE=80;

	public static void main(String[] args)  {
		ArrayList<String> Sample = new ArrayList<String>();
		String ReadFileName = "";
		String OutFileName = "";
		boolean Debug = false;
		try {
			// check if command-line argument is right or not
			if (args.length < 4) {
				System.out.println("ERROR: missing command line argument");
			}
			else if (args.length > 5){
				System.out.println("ERROR: too many command line arguments");
			}
			else {
				//run this program without using debug mode
				if (args.length ==4){
					Debug = false;
					int prefixLength = Integer.valueOf(args[0]);
					int numWords = Integer.valueOf(args[1]);
					ReadFileName = args[2];
					Sample = readSourceFile(ReadFileName);
					// if error check is correct, it will go
					if (errorCheck(prefixLength, numWords,Sample)){
						OutFileName = args[3];			
						WriteToFile(OutFileName, Sample, prefixLength, numWords, Debug);
					}    
				}
				// run this program with debug mode
				else if (args.length==5&&args[0].equals("-d")){
					Debug = true;
					int prefixLength = Integer.valueOf(args[1]);
					int numWords = Integer.valueOf(args[2]);
					ReadFileName = args[3];
					Sample = readSourceFile(ReadFileName);
					// if error check is correct, it will generate words
					if (errorCheck(prefixLength, numWords,Sample)){
						OutFileName = args[4];		
						WriteToFile(OutFileName, Sample, prefixLength, numWords, Debug);
					}
				}
				else {
					System.out.println("ERROR: if using debug mode, command line should be: -d prefixLength numWords sourceFile outputFile");
				}
			}
		}
		catch (NumberFormatException exc){
			System.out.println("PrefixLength and numWords should be an integer");
		}
		catch (FileNotFoundException exc) {
			System.out.println("File not found: " + ReadFileName);
		}
		catch (IOException exc) {
			exc.printStackTrace();
		}
	}
	/**
	 * private method to check whether each argument is a valid type or not
	 * @param prefixLength length of each prefix
	 * @param numWords number of words which will write to file
	 * @param Sample each word as an index of String array from input file
	 * @return false indicating there is something wrong in the command line argument
	 */
	private  static boolean errorCheck(int prefixLength, int numWords, ArrayList<String> Sample ){
		if (prefixLength<1){
			System.out.println("Prefixlength should be greater than 1 and smaller than source file");
			return false;
		}
		if (prefixLength>=Sample.size()){
			System.out.println("Prefixlength should be smaller than source file");
			return false;
		}
		if (numWords<0){
			System.out.println("numWords must be positive");
			return false;
		}
		return true;
	}
	/**
	 * a private method to read the input file
	 * @param fileName name of input file
	 * @return type of arraylist that would be used for generating words
	 * @throws IOException if the file not found
	 */
	private static ArrayList<String> readSourceFile(String fileName) throws IOException {
		String word = "";
	    // create Scanner inFile1
	    Scanner inFile = new Scanner(new File(fileName));
	    ArrayList<String> tempArr = new ArrayList<String>();
	    // while loop
	    while (inFile.hasNext()) {
	      // find next line
	    	word = inFile.next();
	      tempArr.add(word);
	    }
	    inFile.close();
	    return tempArr;
	}
	/**
	 * the method to run a class "RandomTextGenerator" to generate words and write content to file, 
	 * if there is not a permission to write to a file, it will be caught that cannot write to file
	 * and doesn't do anything.
	 * @param OutFileName the name of output file
	 * @param Sample the input words that is a type of arraylist 
	 * @param prefixLength the length of prefix
	 * @param numWords the total number of words that are written to file
	 * @param Debug mode 
	 */
	private static void WriteToFile (String OutFileName, ArrayList<String>Sample, int prefixLength, int numWords, boolean Debug){
		try {
			PrintWriter outputStream = new PrintWriter(OutFileName);
			RandomTextGenerator generator = new RandomTextGenerator(Sample, prefixLength, numWords, Debug);
			String result = generator.RandomGenerate();
			ArrayList<String> resultArr = textLimiter(result);
			for (int i = 0; i<resultArr.size();i++){
				outputStream.println(resultArr.get(i));
			}		    
			outputStream.close();
		}
		catch (FileNotFoundException e){
			System.out.println("Can't write to the file");
		}
	}
	/**
	 * a helper method to limit each line that contains words in output file
	 * @param input the words that is generated need to be written to output file
	 * @return an arraylist that each value of index is the words that will be put into each line of output file
	 */
	private static ArrayList<String> textLimiter(String input) {
	    ArrayList<String> textList = new ArrayList<>();
	    String[] parts = input.split(" ");
	    StringBuilder sb = new StringBuilder();
	    for (String part : parts) {
	        if (sb.length() + part.length() > MAX_PER_LINE) {
	        	textList.add(sb.toString().substring(0, sb.toString().length() - 1));
	            sb = new StringBuilder();
	        }
	        sb.append(part + " ");
	    }
	    if (sb.length() > 0) {
	    	textList.add(sb.toString());
	    }
	    return textList;
	}
}
