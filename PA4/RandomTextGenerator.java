//Name:Yi-An Chen
//USC loginid:4558-3114-48
//CS 455 PA4
//Fall 2015

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 * RandomTextGenerator class 
 * using Using Makarov chain to generate new text by looking more than two connected words
 * using HashMap which paramates are prefix as key and arraylist as values
 * to store all prefix as keys and all possibilities value that may follow that key
 * @author Yi-An Chen
 *
 */
public class RandomTextGenerator {
        private final static int FIX_SEED = 1;
	private HashMap<Prefix, ArrayList<String>> prefixMap;
	private Random rand;
	private ArrayList<String> source;
	private int prefixLength;
	private boolean Debug;
	private int numWords;
	/**
	 * a RandomTextGenerator constructor that carries some parameters from the command-line argument
	 * @param origArr words stored by an arraylist that produced by source file 
	 * @param prefixLength length of prefix 
	 * @param numWords the total number of words written into ouput file
	 * @param Debug mode
	 */
	public RandomTextGenerator (ArrayList<String> origArr, int prefixLength,int numWords, boolean Debug){
		prefixMap = new HashMap<Prefix, ArrayList<String>>();
		this.Debug = Debug;
		source = origArr;
		this.numWords = numWords;
		this.prefixLength = prefixLength;
		if (Debug){
			rand = new Random(FIX_SEED);
		}
		else rand = new Random();
	}
	/*
	 * a method to generate the first prefix 
	 */
	public Prefix firstPrefix(){
		String startInput = "";
		//to generate by random 
		int start = rand.nextInt(source.size()-prefixLength+1);
		for (int i = 0;i<prefixLength; i++){
			startInput = startInput + source.get(start+i) + " ";
		}
		Prefix firstPrefix = new Prefix(startInput).cutSpace();
		return firstPrefix;
	}
	/*
	 * to generate all prefix as keys, then put all possibility values with that key. A data structure of HashMap 
	 */
	public void PrefixGenerate (){	
		for (int i=0; i<source.size()-prefixLength+1;i++){
			// create all prefix determined by prefixLength
			int count = 0;
			String sourceInput ="";
			while(count<prefixLength){
				sourceInput = sourceInput + source.get(i+count) + " ";
				count++;
			}
			/*
			 * if that key of prefix exists, just add the following word as their values
			 * if that key of prefix doesn't exist, put a new key and value
			 */
			Prefix prefix = new Prefix(sourceInput).cutSpace();
			if(prefixMap.containsKey(prefix)){
				if((i+count)<source.size()){
					prefixMap.get(prefix).add(source.get(i+count));
				}
			}
			else {
				prefixMap.put(prefix, new ArrayList<String>());
				if((i+count)<source.size()){
				prefixMap.get(prefix).add(source.get(i+count));
				}
			}
		}	
	}
	/*
	 * starts to generate words randomly by number of numWords 
	 */
	public String RandomGenerate(){
		PrefixGenerate();
		String essay = "";
		Prefix prefixGenerator = firstPrefix();
		if (Debug){
			System.out.println("DEBUG: chose a new initial prefix: " + prefixGenerator.printPrefix());
			System.out.println("DEBUG: prefix: " + prefixGenerator.printPrefix());
		}	
		for (int i=0;i<numWords;i++){
			if (prefixMap.get(prefixGenerator).isEmpty()){
				prefixGenerator = firstPrefix();
				if (Debug){
					System.out.println("DEBUG: successors: <END OF FILE>");
					System.out.println("DEBUG: chose a new initial prefix: " + prefixGenerator.printPrefix());
				}
			}
			else {
				if (Debug){
					System.out.println("DEBUG: successors: " + prefixMap.get(prefixGenerator));
				}
				int sequenceWord = rand.nextInt(prefixMap.get(prefixGenerator).size());					
				prefixGenerator = prefixGenerator.shiftPrefix(prefixMap.get(prefixGenerator).get(sequenceWord));
				essay = essay + prefixGenerator.lastWordPrefix() + " ";
				if (Debug){
					System.out.println("DEBUG: word generated: " + prefixGenerator.lastWordPrefix());
					System.out.println("DEBUG: prefix: " + prefixGenerator.printPrefix());
				}
			}
		}	
		return essay;
	}
}

