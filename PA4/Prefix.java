//Name:Yi-An Chen
//USC loginid:4558-3114-48
//CS 455 PA4
//Fall 2015
/**
 * a prefix class that stores into String words without overwrite hashcode and equals methods
 * prefix would be used for key of hashMap
 * @author Yi-An Chen
 *
 */
public class Prefix {
	private String prefixString;
	
	public Prefix(String input){
		prefixString = input;
	}
	// to cut the rest of space generated when prefixLength more than 1
	public Prefix cutSpace(){
		String prefixWOspace = prefixString.substring(0, prefixString.length()-1);
		return new Prefix(prefixWOspace);
	}
	/*
	 * to shift the prefix by returning a new prefix
	 */
	public Prefix shiftPrefix(String word){
		String shiftPrefix = "";
		String[] arrPrefix = prefixString.split(" ");
		for (int i =1; i<arrPrefix.length;i++){
			shiftPrefix = shiftPrefix + arrPrefix[i] + " ";
		}
		shiftPrefix = shiftPrefix + word;
		return new Prefix(shiftPrefix);
	}
	/*
	 * when prefixLength more than 1, it will add another words to become a prefix
	 */
	public void addPrefix(String word){
		prefixString = prefixString + word;
	}
	/*
	 * return a String word of prefix that can be printed 
	 */
	public String printPrefix(){
		return prefixString;
	}
	/*
	 * help to print the generating word by using Debug mode
	 */
	public String lastWordPrefix(){
		String [] arrPrefix = prefixString.split(" ");
		return arrPrefix[arrPrefix.length-1];
	}
	
	public int hashCode(){

		return prefixString.hashCode();
	}

	public boolean equals(Object obj){
		if (!(obj instanceof Prefix)){
			    return false;
			}
		if (obj==this){
			return true;
			}
		Prefix other=(Prefix) obj;
		return this.prefixString.equals(other.prefixString);
	}
	
}



