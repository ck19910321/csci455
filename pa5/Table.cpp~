// Name:Yi-An Chen
// Loginid:4558-3114-48
// CSCI 455 PA5
// Fall 2015

// Table.cpp  Table class implementation


/*
 * Modified 11/22/11 by CMB
 *   changed name of constructor formal parameter to match .h file
 */

#include "Table.h"

#include <iostream>
#include <string>
#include <cassert>


// listFuncs.h has the definition of Node and its methods.  -- when
// you complete it it will also have the function prototypes for your
// list functions.  With this #include, you can use Node type (and
// Node*, and ListType), and call those list functions from inside
// your Table methods, below.

#include "listFuncs.h"


//*************************************************************************

//Defaulut Table constructor with fixed HashSize
Table::Table() {
	data = new ListType[HASH_SIZE];
	hashSize = HASH_SIZE;
	theLongestChain = 0;
	numEntry = 0;
	nonEmptyBucket = 0;
	for (int i =0;i<hashSize;i++){
		data[i]=NULL;
	}
}

// Table constructor with suer given hashSize
Table::Table(unsigned int hSize) {
	data = new ListType [hSize];
	hashSize = hSize;
	theLongestChain = 0;
	numEntry = 0;
	nonEmptyBucket = 0;
	for (int i=0;i<hashSize;i++){
		data[i]=NULL;
	}

}

//return a value with given a key
int * Table::lookup(const string &key) {
	int hashValue = hashCode(key);
	if (data[hashValue]==NULL){
		return NULL;
	}
	else
		return listLookup(data[hashValue], key);

  //return NULL;   // dummy return value for stub
}

// return iff there is a key existed in the table or not
bool Table::remove(const string &key) {
	int hashValue = hashCode(key);

	if (listRemoveNode(data[hashValue], key)){
		numEntry--;
		if (data[hashCode(key)]==NULL){
			nonEmptyBucket--;
		}
		findChain();
		return true;
	}
	else return false;
}

// insert key and value, if there is no that key in the table, return true, else return false and do not insert that key
bool Table::insert(const string &key, int value) {
	int hashValue = hashCode(key);
	if (data[hashValue]==NULL){      // there is an empty list at index of hashValue, so the number of non-bucket will increase
		listInsert(data[hashValue], key, value);
		nonEmptyBucket++;
		numEntry++;
		findChain();
		return true;
	}
	else if (listLookup(data[hashValue], key)==NULL){  // the list is not empty and didnt find the key, so the number of non-bucket will not increase
		listInsert(data[hashValue], key, value);
		numEntry++;
		findChain();
		return true;
	}
	else
		return false;  // dummy return value for stub
}

// return a number of entries 
int Table::numEntries() const { 
  return numEntry;      // dummy return value for stub
}

//print all key and value in the table
void Table::printAll() const {
	for (int i =0; i<hashSize;i++){
		listPrintList(data[i]);
	}

}

void Table::hashStats(ostream &out) const {

	out << "number of buckets: " << hashSize << endl;
	out << "number of entries: " << numEntry << endl;
	out << "number of non-empty buckets: " << nonEmptyBucket << endl;
	out << "number of longest chain: " << theLongestChain << endl;
}


// add definitions for your private methods here

// a private function to help calculate the longest chain in the table
void Table::findChain() {
        theLongestChain = 0;
	for (int i =0; i<hashSize;i++){
	   unsigned int tempChain = 0;
	   if (data[i]!=NULL){           // the list is not empty so the chain will be started from 1
		        tempChain =1;
			Node *temp = data[i];
			while (temp ->next!=NULL){
				tempChain++;
				temp = temp -> next;
			}
			if (tempChain > theLongestChain){
				theLongestChain = tempChain;
			}
		}
	}
}
