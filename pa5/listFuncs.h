// Name:Yi-An Chen
// Loginid:4558-3114-48
// CSCI 455 PA5
// Fall 2015


//*************************************************************************
// Node class definition 
// and declarations for functions on ListType

// Note: we don't need Node in Table.h
// used by the Table class; not by any Table client code.

#ifndef LIST_FUNCS_H
#define LIST_FUNCS_H

using namespace std;

struct Node {
  string key;
  int value;

  Node *next;

  Node(const string &theKey, int theValue);

  Node(const string &theKey, int theValue, Node *n);
};


typedef Node * ListType;

//*************************************************************************
//add function headers (aka, function prototypes) for your functions
//that operate on a list here (i.e., each includes a parameter of type
//ListType or ListType&).  No function definitions go in this file.

// insert a new pair into a given list
void listInsert(ListType &list, string key, int value);

// return the address of value or NULL if the key is not in the given list
int* listLookup(ListType &list, string key);

// remove a given key in the given list 
// return false iff the key is not present
bool listRemoveNode(ListType &list, string key);

//print out all the keys with values per line
//ex: jane 90
//    Ian 100
void listPrintList(ListType &list);

//return true iff the list is empty
bool listIsEmpty(ListType &list);








// keep the following line at the end of the file
#endif

