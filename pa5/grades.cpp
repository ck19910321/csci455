// Name:Yi-An Chen
// Loginid:4558-3114-48
// CSCI 455 PA5
// Fall 2015

/*
 * grades.cpp
 * A program to test the Table class.
 * How to run it:
 *      grades [hashSize]
 *
 * the optional argument hashSize is the size of hash table to use.
 * if it's not given, the program uses default size (Table::HASH_SIZE)
 *
 */

#include "Table.h"

// cstdlib needed for call to atoi
#include <cstdlib>

// a method to help print command
void printCommand(){
	cout<<"Command: "<<endl;
	cout<<"insert, change, lookup, ";
	cout<<"remove, print, ";
	cout<<"size, stats, ";
	cout<<"help, quit "<<endl;
}

// a helper method to prompt a name 
string enterName(){
	string name;
	//cout<<"please enter a name"<<endl;
	cin>>name;
	return name;
}

// a helper method to prompt a score
int enterScore(){
	int  score;
	//cout<<"please enter a score"<<endl;
	cin>>score;
	return score;
}

// a method to insert a name 
//if there is present will print out a message
void doInsert(string name, int score, Table* grades){
	if(!grades->insert(name, score)){
		cout<<"the name is already existed"<<endl;
	}
}

// a method to change a score with given name 
// if there is not the name will print out a message
void doChange(string name, int score, Table* grades){
	if (grades->lookup(name)){
		int *value=grades->lookup(name);
		/*int *tempScore=new int();
		tempScore = score;*/
		*value = score;
	}
	else
		cout<<"the name is not existed"<<endl;
}

// a method to lookup a name and then print out that value
// if there is not the name it will print out the message
void doLookup(string name, Table* grades){
	if(grades->lookup(name)){
		int* value = grades->lookup(name);
		cout<<"the score is: "<<*value<<endl;
	}
	else
		cout<<"the name is not existed"<<endl;
}

// a method to remove a name 
// if there is no the name it will print out a message
void doRemove(string name, Table* grades){
	if(grades->remove(name)){
		cout<<"remove this name successfully"<<endl;
	}
	else
		cout<<"the name is not existed"<<endl;
}

// a helper method to print out cmd> then pronmt a command
string cmdCommand(){
	cout<<"cmd>";
	string command;
	cin>>command;
	return command;
}

// a main method to call all method, if the command is not correct, it will print out a error message
void doCommand(Table *grades){
          string command;
	  bool close = true;
	  do {
	    //printCommand();
	    command = cmdCommand();
	    if (cin.fail()) {
	      cout << "ERROR: input stream failed." << endl;
	      close = false;
	    }
	    else if (command=="insert"){
	      	string name = enterName();
	      	int score = enterScore();
	      	doInsert(name, score, grades);
	      	
	     }
	    else if (command=="change"){
	      		string name = enterName();
	      		int score = enterScore();
	      		doChange(name, score, grades);
	      		
	      }
	    else if (command=="lookup"){
	      		string name = enterName();
	      		doLookup(name, grades);
	      		
	      }
	    else if (command=="remove"){
	      		string name = enterName();
	      		doRemove(name, grades);
	      		
	    }
	    else if (command=="print"){
	    	  	grades->printAll();
	      		
	    }
	    else if (command=="size"){
	    	  	cout<<"the number of entries is "<<grades->numEntries()<<endl;
	      		
	    }
	    else if (command=="stats"){
	      		grades->hashStats(cout);
	      		
	    }
	    else if (command=="help"){
	      		printCommand();
	      		
	    }
	    else if (command=="quit"){
	    	  	close=false;
	      		
	    }
	    else {
	      cout<<"ERROR: invalid command, type [help] to see brief command"<<endl;
	    }
	      }while (close);
}
int main(int argc, char * argv[]) {

  // gets the hash table size from the command line

  int hashSize = Table::HASH_SIZE;

  Table * grades;  // Table is dynamically allocated below, so we can call
                   // different constructors depending on input from the user.

  if (argc > 1) {
    hashSize = atoi(argv[1]);  // atoi converts c-string to int

    if (hashSize < 1) {
      cout << "Command line argument (hashSize) must be a positive number"
	   << endl;
      return 1;
    }

    grades = new Table(hashSize);

  }
  else {   // no command line args given -- use default table size
    grades = new Table();
  }

  grades->hashStats(cout);

  // add more code here
  // Reminder: use -> when calling Table methods, since grades is type Table*
  doCommand(grades);
  return 0;
}
