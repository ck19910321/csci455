// Name: Yi-An Chen
// Loginid: 4558-3114-48
// CSCI 455 PA5
// Fall 2015

#include <iostream>

#include <cassert>

#include "listFuncs.h"

using namespace std;

Node::Node(const string &theKey, int theValue) {
  key = theKey;
  value = theValue;
  next = NULL;
}

Node::Node(const string &theKey, int theValue, Node *n) {
  key = theKey;
  value = theValue;
  next = n;
}


ListType list;


//*************************************************************************
// put the function definitions for your list functions below
void listInsert(ListType &list, string key, int value){
	
        // if the list is empty
        if (list ==NULL){       
		Node *p = new Node (key, value);
		list = p;
	}
        // if the list is not empty, it will create a node then point to the list
	else {
		Node *p = new Node(key, value, list);
		list = p;
	}
}

int* listLookup(ListType &list, string key){
        
	Node *p = list;
	while(p!=NULL){
		if (p->key==key){ 
			return &p->value;
		}
		p = p->next;    // to find the key in the list one by one
	}
	return NULL;            // return NULL if there is not found 
}

bool listRemoveNode(ListType &list, string key){
        //if given list is empty, reutrn false indicating the key is not present  
        if (list == NULL) {       
		 return false;
	}
	Node *p = list;
	if (p->key==key){        //remove the first element
		Node *temp = p;
		list = p->next;
		delete temp;     //delete first node
	    return true;
	}
	while(p->next!=NULL){
		if (p->next->key ==key){
			Node *temp = p->next;
			Node *q = temp;
			if (temp->next==NULL){  //if target at last element
				p->next=NULL;
				delete temp;
				return true;
			}
			else{                   //if target at middle of list
				p->next = q->next;
				delete temp;
				return true;
			}
		}
		p = p->next;
	}
	return false;
}


void listPrintList(ListType &list){
	  if (list == NULL) {
		  return;
	  }
	  Node *p = list;
	  while (p != NULL) {
		  cout << p->key << " ";
		  cout << p->value <<endl;
		  p = p->next;
	  }
}
bool listIsEmpty(ListType &list){
	if (list==NULL){
		return true;
	}
	else return false;
}
