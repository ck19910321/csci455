// Name: Yi-An Chen
// USC loginid: chenyian
// CSCI455 PA2
// Fall 2015


/**
   <add main program comment here>
 */
import java.util.Scanner;

public class BulgarianSolitaireSimulator {

   public static void main(String[] args) {
     
      boolean singleStep = false;
      boolean userConfig = false;

      for (int i = 0; i < args.length; i++) {
         if (args[i].equals("-u")) {
            userConfig = true;
         }
         else if (args[i].equals("-s")) {
            singleStep = true;
         }
      }

      // <add code here>
      SolitaireBoard SBoard;
      if (userConfig == true){
    	   SBoard = new SolitaireBoard(userConfig());
    	   if (singleStep){
    		   simulateWithStep(SBoard);
    	   }
    	   else
    		   simulateWoStep(SBoard);
    		   
      }
      else {
    	  SBoard = new SolitaireBoard();
    	   if (singleStep){
    		   simulateWithStep(SBoard);
    	   }
    	   else 
    		   simulateWoStep(SBoard);
      }
   }
   
    // <add private static methods here>
   /**
    * create a method for client to prompt the number in a line and check if it is satisfied the rule of SolitaireBoard.
    * @return the client's valid number
    */
   private static String userConfig(){
	   boolean ok = false;
	   String numberString = "";
	   Scanner in = new Scanner(System.in);
	   System.out.println("Number of total cards is " + SolitaireBoard.CARD_TOTAL);
	   System.out.println("You will be entering the initial configuration of the cards (i.e., how many in each pile).)");
	   while(!ok){
		   numberString = in.nextLine();
		   System.out.println("Please enter a space-separated list of positive integers followed by newline: ");
		   ok = SolitaireBoard.isValidConfigString(numberString);
		   if (!ok){
			   System.out.println("ERROR: Each pile must have at least one card and the total number of cards must be " + SolitaireBoard.CARD_TOTAL);
		   }
	   }
	   return numberString;
   }
   /**
    * create a method that simulates the game until the game is done
    * @param board an client's initial configuration 
    */
  private static void simulateWoStep(SolitaireBoard SBoard){
	  Scanner in = new Scanner(System.in);
	  int k =0;
	  System.out.println("Initial configuration: " + SBoard.configString());
	  while (!SBoard.isDone()){
		  k++;
		  SBoard.playRound();
		  System.out.println("[" + k + "] Current configuration: " + SBoard.configString());
	  }
	  System.out.println("Done!");
  }
  /**
   * create a method that simulates the game and stops each step
   * @param board board an client's initial configuration 
   */
  private static void simulateWithStep(SolitaireBoard SBoard){
	  Scanner in = new Scanner(System.in);
	  int k =0;
	  System.out.println("Initial configuration: " + SBoard.configString());
	  while (!SBoard.isDone()){
		  k++;
		  SBoard.playRound();
		  System.out.println("[" + k + "] Current configuration: " + SBoard.configString());
			  System.out.println("<Type return to continue>");
			  in.nextLine();
		  }
	  System.out.println("Done!");
  }
  
}
