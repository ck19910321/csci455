//Name: Yi-An Chen
//USC loginid: chenyian
//CSCI455 PA2
//Fall 2015

/*
   class SolitaireBoard
   The board for Bulgarian Solitaire.  You can change what the total number of cards is for the game
   by changing NUM_FINAL_PILES, below.  Don't change CARD_TOTAL directly, because there are only some values
   for CARD_TOTAL that result in a game that terminates.
   (See comments below next to named constant declarations for more details on this.)
 */

import java.util.Random;

public class SolitaireBoard {
   
   public static final int NUM_FINAL_PILES = 9;
   // number of piles in a final configuration
   // (note: if NUM_FINAL_PILES is 9, then CARD_TOTAL below will be 45)
   
   public static final int CARD_TOTAL =NUM_FINAL_PILES * (NUM_FINAL_PILES + 1) / 2;
   // bulgarian solitaire only terminates if CARD_TOTAL is a triangular number.
   // see: http://en.wikipedia.org/wiki/Bulgarian_solitaire for more details
   // the above formula is the closed form for 1 + 2 + 3 + . . . + NUM_FINAL_PILES
   
   
   /**
      Representation invariant:
    * All numbers in array must be greater than 0, no negative values
    * The totoal value for all numbers is 45
    * Each value is no more than 45 
    * CardArray.length is 45
    */
   
   // <add instance variables here>
   private int[] cardsArr;
   private Random rand;
   /**
     Initialize the board with the given configuration.
     PRE: SolitaireBoard.isValidConfigString(numberString)
   */
   public SolitaireBoard(String numberString) {
	  cardsArr = new int[CARD_TOTAL]; // create an initial configuration
	  String [] newNumberString = numberString.split(" "); // convert String number into Integer number
	  for (int i =0;i<newNumberString.length;i++){
		  cardsArr[i] = Integer.valueOf(newNumberString[i]);
      }

      assert isValidSolitaireBoard();   // sample assert statement (you will be adding more of these calls)
   }
 
   
   /**
      Create a random initial configuration.
   */
   public SolitaireBoard() {
	   rand = new Random();
	   int totCard = CARD_TOTAL;
	   cardsArr = new int[CARD_TOTAL]; // create an initail confiquration
	   
	   for (int i =0; i<CARD_TOTAL; i++){
		   int randCard = rand.nextInt(totCard+1); // pick up a random number within 0 to 45
		   cardsArr[i] = randCard;
		   totCard = totCard - randCard;
	   
	   }
	   assert isValidSolitaireBoard();
   }
  
   
   /**
      Play one round of Bulgarian solitaire.  Updates the configuration according to the rules of Bulgarian
      solitaire: Takes one card from each pile, and puts them all together in a new pile.
    */
   public void playRound() {
	   int numsPile = 0;
	   int numOfCards =0;
	   // calculate how many numbers are picked up 
	   for (int i = 0; i<cardsArr.length; i++){
		   if (cardsArr[i]!=0){
		   cardsArr[i] =cardsArr[i]-1;
		   numOfCards++; 
		   }			   
	   }	
	   // calculate how many numbers are greater than zero and put a new piles 
	   for (int i=0;i<cardsArr.length; i++){
		   if (cardsArr[i]!=0){
			   cardsArr[numsPile] = cardsArr[i];
			   numsPile++; 
			   }
		   }
	   cardsArr[numsPile] = numOfCards;	  	
	   // the values after the new pile convert convert to zero
	   for (int k = numsPile+1; k<cardsArr.length; k++){
		   cardsArr[k] = 0;
	   }
	   assert isValidSolitaireBoard();
   }
   
   /**
      Return true iff the current board is at the end of the game.  That is, there are NUM_FINAL_PILES
      piles that are of sizes 1, 2, 3, . . . , NUM_FINAL_PILES, in any order.
    */
   
   public boolean isDone() {
	   // create a new boolean array to check all values from our configuration whether it is done or not 
	   boolean [] check = new boolean[NUM_FINAL_PILES+1];
	   for (int i = 0; i < cardsArr.length; i++){
		   int index = cardsArr[i];
		   if (index == 0) continue;
		   if (index >= NUM_FINAL_PILES+1 || check[index])
			   return false;
		   else
			   check[index]= true;
	   }
	   boolean result = true;
	   for(int i=1; i<check.length; i++)
		   result = result && check[i];
	   return result;
   }

   
   /**
      Returns current board configuration as a string with the format of
      a space-separated list of numbers with no leading or trailing spaces.
      The numbers represent the number of cards in each non-empty pile.
    */
   public String configString() {
	  String output = "";
	  for (int i =0; i<cardsArr.length;i++){
		  if(cardsArr[i]!=0){
			  output = output + cardsArr[i] + " ";
		  }
	  }
	  assert isValidSolitaireBoard();
      return output;   
      
   }
   
   
   /**
      Returns true iff configString is a space-separated list of numbers that
      is a valid Bulgarian solitaire board assuming the card total SolitaireBoard.CARD_TOTAL
   */
   public static boolean isValidConfigString(String configString) {
	   String [] newConfigString = configString.split(" ");
	   int[] tempArr = new int[newConfigString.length];
	   for(int i=0; i<newConfigString.length; i++) {
		   if( isValidInteger(newConfigString[i])) 
			   tempArr[i] = Integer.valueOf(newConfigString[i]);
		   else
			   return false;
	   }
	   
	   return isValidNumArr(tempArr);     
   }


   /**
      Returns true iff the solitaire board data is in a valid state
      (See representation invariant comment for more details.)
    */
   private boolean isValidSolitaireBoard() {
	   return isValidNumArr(cardsArr);

   }
   

    // <add any additional private methods here>
   /**
    * create a method that all check method can use 
    * @param arr the configuration was already constructed
    * @return it is valid or not 
    */
   private static boolean isValidNumArr(int[] arr) {
	   int sum = 0;
	   if (arr.length <= 0) 
		   return false;
	   for (int i=0; i<arr.length; i++) {
		   if (arr[i] < 0 || arr[i] > CARD_TOTAL)
			   return false;
		   sum += arr[i];
	   }
	   if (sum != CARD_TOTAL)
		   return false;
	   return true;
   }
   /**
    * check any data that client prompts for if it is valid or not 
    * @param s the string number that client prompts
    * @return it is valid or not 
    */
   private static boolean isValidInteger(String s) {
	    if(s.isEmpty()) return false;
	    for(int i = 0; i < s.length(); i++) {
	        if(i == 0 && s.charAt(i) == '-') {
	            return false;
	        }
	        if(Character.digit(s.charAt(i),10) < 0) return false;
	    }
	    return true;
	}
}
