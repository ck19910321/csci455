// Name:Yi-An CHen
// USC loginid:chenyian
// CS 455 PA1
// Fall 2015

import java.util.Random;
/**
   Drunkard class
       Represents a "drunkard" doing a random walk on a grid.
*/

public class Drunkard {
    private int theStepSize;
    private ImPoint startLoc;
	  
    /**
       Creates drunkard with given starting location and distance
       to move in a single step.
       @param startLoc starting location of drunkard
       @param theStepSize size of one step in the random walk
     */
    public Drunkard(ImPoint startLoc, int theStepSize) {
	this.startLoc = startLoc;
    	this.theStepSize = theStepSize;  	
    }

    /**
       Takes a step of length step-size (see constructor) in one of
       the four compass directions.  Changes the current location of the drunkard.
    */
    public void takeStep() {
	Random rand= new Random();
    	int direction;
    	direction = rand.nextInt(4);;
       	switch (direction){
	case 0:
	    this.startLoc= this.startLoc.translate(0, this.theStepSize);
	    break;
	case 1:		
	    this.startLoc = this.startLoc.translate(0, -this.theStepSize);
    	    break;
    	case 2:
	    this.startLoc = this.startLoc.translate(this.theStepSize, 0);
    	    break;
    	case 3:
    	    this.startLoc = this.startLoc.translate(-this.theStepSize, 0);
    	     break;
    			 }
    }

    /**
       gets the current location of the drunkard.
       @return an ImPoint object representing drunkard's current location
    */
    public ImPoint getCurrentLoc() {
	return new ImPoint(this.startLoc.getX(),this.startLoc.getY());	
    }

}
