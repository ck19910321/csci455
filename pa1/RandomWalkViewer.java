// Name:Yi-An CHen
// USC loginid:chenyian
// CS 455 PA1
// Fall 2015
import javax.swing.JFrame;
import java.util.Scanner;
public class RandomWalkViewer {
	/**
	   This class is to create a frame 
	   and prompt the number of steps that a drunkard would walk randomly
	   @param args not used
	 */
	public static void main(String[] args){
	    JFrame frame = new JFrame();
	    frame.setSize(400, 400);
	    frame.setTitle("RandomWalk");
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    frame.setVisible(true);
	    Scanner input = new Scanner (System.in);
		
	    // To examine whether the steps would be positive or negative
	    while(true){
	       	System.out.printf("Enter the number of steps: ");
	       	int steps = input.nextInt();
	       	RandomWalkComponent rwc = new RandomWalkComponent(steps);
	       	frame.add(rwc);
		frame.repaint();
	       	frame.setVisible(true);
	       	if (steps >0) {
	       		break;	
	       	} 
	       	else
	       		System.out.printf("Error!!! Enger the number of steps: ");
		} 
	}
}
