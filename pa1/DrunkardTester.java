// Name:Yi-An CHen
// USC loginid:chenyian
// CS 455 PA1
// Fall 2015
    /**
       a test that would work all method in Drunkard class
    */
public class DrunkardTester {

    /**
       Test driver for Drunkard class.
       @param args not used
    */
    public static void main(String[] args) {


    DrunkTest(0,0, 1);
    
    DrunkTest(100,100, 2);

    }
		
    /**
       Test all Drunkard methods on (x,y) and the StepSize
       @param theStepSize the size of step that a DrunkardTest would move each time
    */
    private static void DrunkTest(int x, int y, int theStepSize) {
	
	ImPoint loc = new ImPoint(x, y);
	
	Drunkard drunkTest = new Drunkard(loc, theStepSize);
	
	System.out.println("DrunkTest begins at " + loc);
	System.out.println("Testing getX, getY...");
	System.out.println("x=" + loc.getX() + " y=" + loc.getY());
	
	testTakeStep(drunkTest, 5, theStepSize); // To examine whether this function can work successfully
	
	System.out.println();
    }

    /**
       Test takeStep method by given theNumStep and theStepSize
       @param drunkTest an object created from Drunkard class
       @param theNumStep the number of steps that would move randomly
       @param theStepSize the size of step that move each time
    */
    private static void testTakeStep(Drunkard drunkTest, int theNumStep, int theStepSize) {
    
    System.out.println("The number of stpes that drunkard would take is: " + theNumStep);
    System.out.println("The size of step that drunkard would take is :" + theStepSize);
   
	for (int i = 1; i <= theNumStep; i++){
		ImPoint old = drunkTest.getCurrentLoc();
		drunkTest.takeStep();
		ImPoint delta = drunkTest.getCurrentLoc();
		
		//To compared real and expected result
		
		System.out.println("New point [expected:("
				   + old.translate(0, theStepSize) + " or " + old.translate(0, -theStepSize) + " or "
				+ old.translate(theStepSize, 0) +" or " + old.translate(-theStepSize, 0));
		System.out.println("New point is: " + delta);
		}
    }
}
