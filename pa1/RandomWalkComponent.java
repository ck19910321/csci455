// Name:Yi-An CHen
// USC loginid:chenyian
// CS 455 PA1
// Fall 2015
import javax.swing.JComponent;
import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
/**
 	A component that draws a drunkard movement
 */
public class RandomWalkComponent extends JComponent {
	private int steps;
	private int stepSize;
	
         /**
	    Communicate the number of steps by passing it to a RandomWalkComponent constructor
	    @param steps number of steps that a drunkard would walk
	 */
	public  RandomWalkComponent(int steps){
		this.steps = steps;
	}
	
         /**
	    override the paintComponent to draw the random walk 
	    using a Drunkard object to keep track of current state of drunkard
	 */ 
	public void paintComponent(Graphics g) {
	    Graphics2D g2 = (Graphics2D) g;
		
	    ImPoint impoint = new ImPoint(200,200);
	    
	    Drunkard drunkard = new Drunkard(impoint, stepSize);
	    
	    stepSize = 5;
		
	    // To draw all of points that a drunkard walked into a line
	    for (int i = 0; i <= steps; i++){
		Point2D.Double from = new Point2D.Double(drunkard.getCurrentLoc().getX(),drunkard.getCurrentLoc().getY());
		drunkard.takeStep();
	      	Point2D.Double to = new Point2D.Double(drunkard.getCurrentLoc().getX(),drunkard.getCurrentLoc().getY());
	       	Line2D.Double segment = new Line2D.Double(from, to);
	       	g2.draw(segment);
		}
	}
}
